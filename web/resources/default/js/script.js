$(document).ready(function(){


    $("#formulaire_inscription").validate({

        rules: {
            nom: {
                require: true,
                minLength: 3,
                maxlength: 15
            },
            prenom: {
                require: true,
                minLength: 3,
                maxlength: 15
            }
        }
    });
})