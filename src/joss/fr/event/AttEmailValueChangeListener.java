package joss.fr.event;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.event.ValueChangeEvent;

@ManagedBean
@SessionScoped
public class AttEmailValueChangeListener {
    public void changeContenuEmail(ValueChangeEvent event){
        UIComponent component = event.getComponent().findComponent("email");
        System.out.println(event.getComponent().getId());
        System.out.println(component.toString());
    }
}
