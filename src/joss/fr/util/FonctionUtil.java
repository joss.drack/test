package joss.fr.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FonctionUtil {

    /*
        Formater une string en format date
     */
    public static Date formatStringDate(String format, String madate){
        Date reformatted;
        SimpleDateFormat fromUser = new SimpleDateFormat(format);

        try {
            reformatted = fromUser.parse(madate);
            return reformatted;
        } catch (ParseException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /*
        Creation d'un numero de commande

     */
    public String createNumeroCommande() {

        Date d = new Date();
        String num = new SimpleDateFormat("yyMMdd").format(d);

        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int charLength = chars.length();
        String numCommande;

        StringBuilder  pass = new StringBuilder (charLength);
        for (int x = 0; x < 4; x++) {
            int i = (int) (Math.random() * charLength);
            pass.append(chars.charAt(i));
        }

        numCommande = num+pass.toString();

        return numCommande;

    }

}
