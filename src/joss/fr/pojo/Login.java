package joss.fr.pojo;

public class Login {

    private int idLog;
    private String login;
    private String password;

    public Login() {}

    public Login(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Login(int idLog, String login, String password) {
        this.idLog = idLog;
        this.login = login;
        this.password = password;
    }

    public int getIdLog() {
        return idLog;
    }

    public void setIdLog(int idLog) {
        this.idLog = idLog;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
