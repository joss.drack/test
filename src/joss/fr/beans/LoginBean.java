package joss.fr.beans;

import joss.fr.pojo.Login;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;


@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String login;
    private String password;

    public String getLogin() {
        System.out.println( "in getLogin" );
        return login;
    }

    public void setLogin(String login) {
        System.out.println( "in setLogin with " + login );
        this.login = login;
    }

    public String getPassword() {
        System.out.println( "in getPassword" );
        return password;
    }

    public void setPassword(String password) {
        System.out.println( "in setPassword with " + password );
        this.password = password;
    }

    //Validateur formulaire champs password
    public void validPassword(FacesContext context, UIComponent component, Object value)
        throws ValidatorException{
        String inputValue = (String) value;
        if (! inputValue.matches("^[^\\s]{4}$")){
            FacesMessage msg = new FacesMessage( "Mot de passe nom valide" );
            msg.setSeverity( FacesMessage.SEVERITY_ERROR );
            throw new ValidatorException(msg);
        }
    }


    public String returnAction() {
        System.out.println( "in returnAction" );

        Login u = new Login(login,password);
        return u.getLogin().equals( "joss" ) && u.getPassword().equals( "joss" )
                ? "home.xhtml?faces-redirect=true"
                : "failure";
    }
}
