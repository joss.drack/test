package joss.fr.beans;

import joss.fr.pojo.Adherent;
import joss.fr.util.FonctionUtil;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@ManagedBean
@SessionScoped
public class GestionUserBean implements Serializable {

    private static final long serialVersionUID = 1L;


    private String nom;
    private String prenom;
    private String email;
    private Date dateNaissance;
    private String sexe;
    private int nbrEnfant;
    private List<Adherent> listAdherents = new ArrayList<Adherent>();
    private int index = 0;

    public GestionUserBean() {}

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public int getNbrEnfant() {
        return nbrEnfant;
    }

    public void setNbrEnfant(int nbrEnfant) {
        this.nbrEnfant = nbrEnfant;
    }

    public List<Adherent> getListAdherents() {
        return listAdherents;
    }

    public void setListAdherents(List<Adherent> listAdherents) {
        this.listAdherents = listAdherents;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<Adherent> initAdherents() {
        Adherent adherent = new Adherent();
        adherent.setNom("Papaya");
        adherent.setPrenom("Merlyne");
        adherent.setDateNaissance(FonctionUtil.formatStringDate("dd/MM/YYYY","23/04/2000"));
        adherent.setSexe("Femme");
        adherent.setNbrEnfant(1);
        listAdherents.add(adherent);

        Adherent adherent1 = new Adherent();
        adherent1.setNom("Drack");
        adherent1.setPrenom("Josué");
        adherent1.setDateNaissance(FonctionUtil.formatStringDate("dd/MM/YYYY","22/05/1992"));
        adherent1.setSexe("Homme");
        adherent1.setNbrEnfant(2);
        listAdherents.add(adherent1);


        Adherent adherent2 = new Adherent();
        adherent2.setNom("Pham");
        adherent2.setPrenom("Sonia");
        adherent2.setDateNaissance(FonctionUtil.formatStringDate("dd/MM/YYYY","07/08/2002"));
        adherent2.setSexe("Femme");
        adherent2.setNbrEnfant(2);
        listAdherents.add(adherent2);

        Adherent adherent3 = new Adherent();
        adherent3.setNom("Assor");
        adherent3.setPrenom("Thomas");
        adherent3.setDateNaissance(FonctionUtil.formatStringDate("dd/MM/YYYY","11/12/1980"));
        adherent3.setSexe("Homme");
        adherent3.setNbrEnfant(3);
        listAdherents.add(adherent3);

        return listAdherents;
    }

    public void initeUptateUser(){

         nom = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nom");
         prenom = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("prenom");
         String param1 = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("dateNaissance");
        dateNaissance = FonctionUtil.formatStringDate("dd/MM/YYYY",param1);
        sexe = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sexe");
        nbrEnfant = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nbrEnfant"));
        index = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("index"));


    }

    public String executeInscrpition(){

        Adherent adherent = new Adherent();
        System.out.println( "Liste des valeur = > " + nom + " " + prenom + " " + dateNaissance + " " + sexe + " " + nbrEnfant);
        adherent.setNom(nom);
        adherent.setPrenom(prenom);
        adherent.setDateNaissance(dateNaissance);
        adherent.setSexe(sexe);
        adherent.setNbrEnfant(nbrEnfant);
        listAdherents.add(adherent);
        System.out.println(adherent);
        FacesMessage message = new FacesMessage( "Succès de l'inscription !" );
        FacesContext.getCurrentInstance().addMessage( null, message );
        return "gestionUser.xhtml?faces-redirect-true";
    }
}
